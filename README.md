# ITV - Concurrent VideoPlays
## Ash Prince

### Brief
We have a business requirement to report on the success of an ITV Hub programme launch. 

Given information about streaming video plays, find the maximum number of video plays that were playing concurrently.

Write Scala, Java or Python code that accepts a finite collection of video play records and returns the maximum that were playing at once.

You can assume:
* All end times are after their corresponding start time
* Each play lasts at most a few hours
* All of the plays happen within one calendar month

### Instructions

To run, navigate to the base of the repository and execute the following:

`python3 ./run.py -f/--file ${path_to_input_json_file}`

By default, output is simply the number of concurrent video plays. However, you can include the `-r/--ranked` flag to
pretty-print a dictionary of the overlap intervals and how many VideoPlay records overlap with each.

`python3 ./run.py -f ${path_to_input_json_file} -r/--ranked`
