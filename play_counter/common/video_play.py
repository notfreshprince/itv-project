from dataclasses import dataclass
from pandas import Timestamp


@dataclass(frozen=True)
class VideoPlay:
    """
    VideoPlay dataclass
    - Comprised of start_time and end_time
    - Immutable
    """
    start_timestamp: Timestamp
    end_timestamp: Timestamp

    def __post_init__(self):
        """
        Enforce assumption start_time < end_time
        """
        if self.end_timestamp < self.start_timestamp:
            raise ValueError("end_time must be after start_time")
