import logging
import pandas as pd
import json
from typing import List
from play_counter.common.video_play import VideoPlay


# Constants
INTERVAL = "interval"
INTERSECTION = "intersection"


LOGGER = logging.getLogger()
LOGGER.setLevel(logging.DEBUG)


class PlayCounter:
    def __init__(self, debug: bool=False):
        self.intervals: pd.DataFrame
        self.overlap_intervals: pd.DataFrame
        self.overlap_indices: dict = {}
        self.overlap_timestamps: dict = {}
        self.results: dict = {}

        self.debug = debug

    def _find_overlaps_by_index(self):
        """
        Duplicates the intervals dataframe in order to iterate over the other
        intervals.

        Intervals are added to the `overlap_indices` dict alongside a list of
        other intervals found to overlap with it. Comparison with the same
        interval is avoided by comparing indices beforehand.
        """
        duplicated_intervals = self.intervals.copy(deep=True)

        for interval_i, interval_i_row in self.intervals.iterrows():
            overlaps = []
            for interval_j, interval_j_row in duplicated_intervals.iterrows():
                if interval_i != interval_j:
                    if interval_i_row[INTERVAL].overlaps(
                            interval_j_row[INTERVAL]
                    ):
                        overlaps.append(interval_j)
            self.overlap_indices[interval_i] = overlaps

        if self.debug:
            for interval, overlaps in self.overlap_indices.items():
                LOGGER.debug(f"{interval} overlaps with {overlaps}")

    def _overlap_indices_to_timestamps(self):
        """
        Takes the resulting dict from `_find_overlaps_by_index()` and replaces
        the index values with Timestamp values for better human readability.
        """
        for index, overlap_indices in self.overlap_indices.items():
            self.overlap_timestamps[
                self.intervals.iloc[index][INTERVAL]
            ] = [
                self.intervals.iloc[overlap_index][INTERVAL] for overlap_index
                in overlap_indices
            ]

        if self.debug:
            for k, v in self.overlap_timestamps.items():
                LOGGER.debug(k)
                for x in v:
                    LOGGER.debug("\t", x)

    def _get_overlap_intervals(self):
        """
        Flattens the dictionary per-overlap set and finds the latest
        start_datetime and earliest end_datetime; where earliest end is later
        than the established latest start.

        These are the common overlap intervals where concurrent watches are
        occurring across VideoPlay objects.
        """
        overlap_intervals = set()
        for interval_key, overlap_values in self.overlap_timestamps.items():
            all_overlapping_intervals = overlap_values
            all_overlapping_intervals.append(interval_key)

            start_timestamps = [interval.left for interval in
                                all_overlapping_intervals]
            latest_start_timestamp = max(start_timestamps)

            end_timestamps = [interval.right for interval in
                              all_overlapping_intervals if
                              interval.right > latest_start_timestamp]
            earliest_end_timestamp = min(end_timestamps)

            overlap_interval = pd.Interval(
                left=latest_start_timestamp,
                right=earliest_end_timestamp
            )
            overlap_intervals.add(overlap_interval)

        self.overlap_intervals = pd.DataFrame(
            columns=[INTERSECTION],
            data=list(overlap_intervals)
        )

        if self.debug:
            LOGGER.debug(self.overlap_intervals)

    def _count_overlaps(self):
        """
        Given the original VideoPlay records, count how many overlap with each
        of the common overlap intervals.
        """
        intersection_counter = {}
        for _, intersection_row in self.overlap_intervals.iterrows():
            its = intersection_row[INTERSECTION]
            intersection_counter[its] = 0
            for _, interval_row in self.intervals.iterrows():
                ivl = interval_row[INTERVAL]
                if its.overlaps(ivl):
                    intersection_counter[its] += 1

        self.results = intersection_counter

    @staticmethod
    def get_max_concurrent_plays(results):
        all_values = results.values()
        max_concurrent = max(all_values)

        return max_concurrent

    def count(self, video_plays: List[VideoPlay]) -> dict:
        """
        Takes a list of VideoPlay dataclass objects, puts them into Interval
        objects and puts the intervals into a dataframe for processing by other
        functions in order to find the maximum concurrent video plays.

        :param video_plays: List of data objects comprised of start & end dates
        :return: dict: Counter-like dictionary outlining most common periods of
                        concurrent plays
        """
        intervals = []
        for record in video_plays:
            i = pd.Interval(left=record.start_timestamp,
                            right=record.end_timestamp,
                            closed="both"
                            )
            intervals.append(i)

        self.intervals = pd.DataFrame(
            columns=[INTERVAL],
            data=intervals
        )

        self._find_overlaps_by_index()
        self._overlap_indices_to_timestamps()
        self._get_overlap_intervals()
        self._count_overlaps()

        return self.results
