import logging
import sys
import json
from pandas import Timestamp
from os.path import abspath
from argparse import ArgumentParser
from play_counter.common.video_play import VideoPlay
from play_counter.common.play_counter import PlayCounter


LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


def _parse_args(args):
    parser = ArgumentParser()
    parser.add_argument("-f",
                        "--file",
                        help="Input filepath containing dates",
                        required=True,
                        dest="file_path")
    parser.add_argument("-r",
                        "--ranked",
                        help="Output ranked concurrent plays",
                        required=False,
                        action="store_true",
                        dest="ranked")
    parser.add_argument("-d",
                        "--debug",
                        help="Run with verbose logging",
                        required=False,
                        action="store_true",
                        dest="debug")
    return parser.parse_args(args)


def main():
    options = _parse_args(sys.argv[1:])

    with open(abspath(options.file_path), "r") as timestamp_file:
        data = json.load(timestamp_file)

        video_plays = []
        for video_play_json in data:
            video_plays.append(
                VideoPlay(
                    Timestamp(video_play_json["start_timestamp"]),
                    Timestamp(video_play_json["end_timestamp"])
                )
            )

        counter = PlayCounter()
        results = counter.count(video_plays=video_plays)

        if options.ranked:
            for k, v in results.items():
                print(f"{k}: {v}")
        else:
            print(counter.get_max_concurrent_plays(results))


if __name__ == '__main__':
    main()
