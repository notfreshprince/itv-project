from unittest import TestCase
from pandas import Timestamp
from play_counter.common.video_play import VideoPlay
from play_counter.common.play_counter import PlayCounter


class Test(TestCase):
    def setUp(self) -> None:
        self.counter = PlayCounter()

    def test_count_single_day(self):
        record_0 = VideoPlay(
            Timestamp(year=2022, month=1, day=1, hour=7, minute=0, second=0),
            Timestamp(year=2022, month=1, day=1, hour=8, minute=30, second=0)
        )
        record_1 = VideoPlay(
            Timestamp(year=2022, month=1, day=1, hour=8, minute=0, second=0),
            Timestamp(year=2022, month=1, day=1, hour=8, minute=45, second=0)
        )
        record_2 = VideoPlay(
            Timestamp(year=2022, month=1, day=1, hour=6, minute=0, second=0),
            Timestamp(year=2022, month=1, day=1, hour=9, minute=45, second=0)
        )
        record_3 = VideoPlay(
            Timestamp(year=2022, month=1, day=1, hour=10, minute=0, second=0),
            Timestamp(year=2022, month=1, day=1, hour=11, minute=30, second=0)
        )
        record_4 = VideoPlay(
            Timestamp(year=2022, month=1, day=1, hour=7, minute=0, second=0),
            Timestamp(year=2022, month=1, day=1, hour=7, minute=15, second=0)
        )
        record_5 = VideoPlay(
            Timestamp(year=2022, month=1, day=1, hour=6, minute=0, second=0),
            Timestamp(year=2022, month=1, day=1, hour=6, minute=15, second=0)
        )
        record_6 = VideoPlay(
            Timestamp(year=2022, month=1, day=1, hour=7, minute=0, second=0),
            Timestamp(year=2022, month=1, day=1, hour=9, minute=15, second=0)
        )

        results = self.counter.count([
            record_0,
            record_1,
            record_2,
            record_3,
            record_4,
            record_5,
            record_6
        ])
        max_concurrent = self.counter.get_max_concurrent_plays(results)

        print(f"Maximum number of concurrent video plays: {max_concurrent}")
        self.assertEqual(4, max_concurrent)

    def test_count_long_range(self):
        record_0 = VideoPlay(
            Timestamp(year=2022, month=1, day=1, hour=6, minute=0, second=0),
            Timestamp(year=2022, month=1, day=1, hour=18, minute=0, second=0)
        )
        record_1 = VideoPlay(
            Timestamp(year=2022, month=1, day=1, hour=17, minute=0, second=0),
            Timestamp(year=2022, month=1, day=2, hour=6, minute=45, second=0)
        )
        record_2 = VideoPlay(
            Timestamp(year=2022, month=1, day=2, hour=9, minute=0, second=0),
            Timestamp(year=2022, month=1, day=2, hour=15, minute=45, second=0)
        )

        results = self.counter.count([
            record_0,
            record_1,
            record_2
        ])
        max_concurrent = self.counter.get_max_concurrent_plays(results)

        print(f"Maximum number of concurrent video plays: {max_concurrent}")
        self.assertEqual(2, max_concurrent)
