from unittest import TestCase
from datetime import datetime, timedelta
from play_counter.common.video_play import VideoPlay


class TestVideoPlay(TestCase):
    def test_init_throws_exception(self):
        """
        Asserts that VideoPlay will raise a ValueError if end_time < start_time
        """
        now = datetime.now()
        yesterday = now - timedelta(days=1)

        self.assertRaises(ValueError, VideoPlay, now, yesterday)
